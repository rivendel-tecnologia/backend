** Maintenance API **

====================
#### Considerações Gerais
Você deverá usar este repositório para o projeto. É permitido usar qualquer linguagem. Precisamos rodar seu código no Ubuntu, esperamos que você crie um README explicando como fazer. Considere que temos instalado: Java, Node, Python, Ruby ou Go. Qualquer outra dependência deve estar no README.

**O desafio é criar uma API de manutenção de veículos.**

=====================

#### Regras gerais

* Os IDs devem ser no formato [UUID] (https://en.wikipedia.org/wiki/Universally_unique_identifier)
* Usar formato JSON para leitura e escrita. (Exemplo: `GET /vehicles/` retornando, `{name: 'my-vehicle, vin: 1234567890'}`)
* A API deverá suportar no mínimo 300 requests/seg.

#### Rotas

##### `/vehicles/`

A entidade `Vehicle` possui os seguintes atributos:

* `id` [type: uuid, mandatory: true]
* `name` [type: string, mandatory: true, min-size: 3, max-size: 10]
* `make` [type: string, mandatory: true, min-size: 0, max-size: 255]
* `model` [type: string, mandatory: true, min-size: 0, max-size: 255]
* `year` [type: string, mandatory: true]
* `vin` [type: string, mandatory: true, min-size: 0, max-size: 255]

Esperamos as seguintes ações:

- `GET /vehicles`  - obtém a lista de veículos
- `POST /vehicles` - cria um `Vehicle`

* * *

##### `/services/`

A entidade `Service` possui os seguintes atributos:

* `id` [type: uuid, mandatory: true]
* `name` [type: string, mandatory: true, min-size: 3, max-size: 10]
* `description` [type: string, mandatory: false, min-size: 0, max-size: 255]


Espera-se as seguintes ações:

- `GET /vehicles/:vehicle_slug/services` - Obtém a lista de serviços de um `Vehicle`
- `POST /vehicles/:vehicle_slug/services` - Cria um `Service` para um `Vehicle`

* * *

##### `/maintenances/`

A entidade `Maintenance` possui os seguintes atributos:

* `status` [type: string, mandatory: true]
* `started_at` [type: date, mandatory: true]
* `finished_at` [type: date, mandatory: true]

*Regras para a rota*

- `started_at`, `finished_at` no formato [ISO 8601]

Espera-se as seguintes ações:

- `GET /maintenances/:vehicle_slug` - Obtém as manutenções de um `Vehicle` e os serviços referentes
- `POST /maintenances/:vehicle_slug` - Cria uma manutenção para um `Vehicle` com os serviços referentes

*Regras para a rota*

- Deve ser possivel finalizar uma manutenção através de uma chamada de API
- Não permitir que o status de uma manutenção seja alterado depois de finalizada
- Não permitir que seja definida mais de uma manutenção com a mesma data de início
- `started_at` deve ser menor que `finished_at`

===============================================

#### O que será avaliado?

* Atende aos requisitos básicos
* Qualidade do código
* Testes
* Automação
* Escalabilidade
* Rodando em Docker

